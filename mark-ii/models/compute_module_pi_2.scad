PANEL_HEIGHT = 80;
PANEL_WIDTH = 20;
BASE_LENGTH = 95;
SWITCH_HOLE = 5;
LED_HOLE = 5;
ROD_DIAMETER = 4.75;

$fn=50;

difference(){
    union(){
        // base
        cube([BASE_LENGTH,3,PANEL_WIDTH]);

        // brace
        translate([0,0,PANEL_WIDTH-2]){
            linear_extrude(height=2){
                polygon(points=[[0,3],[BASE_LENGTH,PANEL_HEIGHT],[BASE_LENGTH,PANEL_HEIGHT-12],[12,3]],
                                paths=[[0,1,2,3]]);
            }
        }

        // panel
        translate([BASE_LENGTH,0,0]){
            difference(){
                // face
                cube([3,PANEL_HEIGHT,PANEL_WIDTH]);
                
                // panel holes
                // power
                translate([-1,15,PANEL_WIDTH/2]){
                    rotate([0,90,0]){
                        cylinder(r=(SWITCH_HOLE/2),h=(3/2)+3);
                    }
                }
                
                // boot
                translate([-1,30,PANEL_WIDTH/2]){
                    rotate([0,90,0]){
                        cylinder(r=(SWITCH_HOLE/2),h=(3/2)+3);
                    }
                }
                
                // ready
                translate([-1,45,PANEL_WIDTH/2]){
                    rotate([0,90,0]){
                        cylinder(r=(LED_HOLE/2),h=(3/2)+3);
                    }
                }
                
                // network
                translate([-1,55,PANEL_WIDTH/2]){
                    rotate([0,90,0]){
                        cylinder(r=(LED_HOLE/2),h=(3/2)+3);
                    }
                }
                
                // overheat
                translate([-1,65,PANEL_WIDTH/2]){
                    rotate([0,90,0]){
                        cylinder(r=(LED_HOLE/2),h=(3/2)+3);
                    }
                }  
            }
        }

        // 40 pin female holder
        translate([10,3,PANEL_WIDTH-15]){
            difference(){
                cube([65,7,15]);
                
                // connector cut-out
                translate([4.5,0,2]){
                    #cube([56,8,11]);
                }
                
                // header cut-out
                translate([6.5,0,-1]){
                    cube([52,8,5]);
                }        
            }
        }

        // bushings(?)
        translate([BASE_LENGTH-2,(ROD_DIAMETER+3)/2,0]){
            cylinder(r=(ROD_DIAMETER+3)/2,h=PANEL_WIDTH);
            translate([0,PANEL_HEIGHT-(ROD_DIAMETER+5),0]){
                cylinder(r=(ROD_DIAMETER+3)/2,h=PANEL_WIDTH);
            }
        }
    }
    
    // bushing holes
    translate([BASE_LENGTH-2,(ROD_DIAMETER+3)/2,-1]){
        cylinder(r=(ROD_DIAMETER+.5)/2,h=PANEL_WIDTH+2);
        translate([0,PANEL_HEIGHT-(ROD_DIAMETER+5),0]){
            cylinder(r=(ROD_DIAMETER+.5)/2,h=PANEL_WIDTH+2);
        }
    }
}