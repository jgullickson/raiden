BASE_THICKNESS = 5;
MOUNTING_HOLE_DIAMETER = 4.5;
BOLT_HEAD_DIAMETER=8.5;
BOLT_HEAD_HEIGHT = 3;
40_PIN_WIDTH = 55.5 ;
40_PIN_HEIGHT = 6.9;
40_PIN_DEPTH = 12.3;
SLOT_LENGTH = 70;

$fn=50;

// base
difference(){

    union(){
            
        // plate
        //cube([(127+MOUNTING_HOLE_DIAMETER*2),100,3]);
        translate([0,0,0]){
            cube([160,100,BASE_THICKNESS]);
        }

        // slots
        // TODO: these sould be sized to fit a standard 40-pin connector 
        translate([8,0,0]){
            for(a=[0:7]){
                translate([20*a,100-SLOT_LENGTH,BASE_THICKNESS]){
                    difference(){
                        cube([8,SLOT_LENGTH,8]);
                        translate([-1,(SLOT_LENGTH/2)-(40_PIN_WIDTH/2),1]){
                            cube([40_PIN_DEPTH,40_PIN_WIDTH,40_PIN_HEIGHT+1]);
                        }
                    }
                }
            }
        }    
    }
    
    // mounting holes
    translate([MOUNTING_HOLE_DIAMETER+16, 40, -1]){
        // hole
        cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=BASE_THICKNESS+2);
        // countersink
        translate([0,0,BASE_THICKNESS-BOLT_HEAD_HEIGHT]){
            cylinder(r=BOLT_HEAD_DIAMETER/2,h=15);
        }
        translate([20,54,0]){
            cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=BASE_THICKNESS+2);
            // countersink
            translate([0,0,BASE_THICKNESS-BOLT_HEAD_HEIGHT]){
                cylinder(r=BOLT_HEAD_DIAMETER/2,h=15);
            }
        }
        translate([127,0,0]){
            cylinder(r=MOUNTING_HOLE_DIAMETER/2,h=BASE_THICKNESS+2);
            // countersink
            translate([0,0,BASE_THICKNESS-BOLT_HEAD_HEIGHT]){
                cylinder(r=BOLT_HEAD_DIAMETER/2,h=15);
            }
        }
    }
}