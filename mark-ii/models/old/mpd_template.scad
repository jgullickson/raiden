PLATE_LENGTH = 100;
PANEL_HEIGHT = 90;
MODULE_WIDTH = 20;
BASE_THICKNESS = 3;
BOARD_EDGE_TO_CONNECTOR_DISTANCE = 40;
ROD_DIAMETER = 3.40;
ROD_CLEARANCE = 1;
LED_SIZE = 5;
// TODO: max height below isn't quite right, debug calculation
LED_MAX_HEIGHT = ROD_DIAMETER*2+(BASE_THICKNESS*2);
LED_COUNT = 5;
// TODO: spacing should be a function of panel height...
LED_SPACING = LED_SIZE*1.5;
SWITCH_COUNT = 2;
SWITCH_SIZE = 5;
SWITCH_SPACING = SWITCH_SIZE*2.5; 
BRACE_THICKNESS = 2.5;

$fn=50;

// *** DESIGN AIDES (REMOVE BEFORE PRINTING!!!) ***
/*
// print bed
translate([50,MODULE_WIDTH/2,-1]){
    color("LightBlue"){
        cylinder(r=55,h=1);
    }
}

// PINE A64 board (dimensions orders as specified on datasheet)
translate([BASE_THICKNESS+1, 19+1, BASE_THICKNESS+1]){
    rotate([90,0,0]){
        color("PaleGreen"){
            cube([133,80,19]);
        }
    }
}
*/
// *** END DESIGN AIDS ***

// ** BEGIN MODEL ***
difference(){
    union(){
        // plate
        translate([0,0,0]){
            cube([PLATE_LENGTH,MODULE_WIDTH,BASE_THICKNESS]);
        }

        // panel
        translate([0,0,0]){
            
            difference(){
                cube([BASE_THICKNESS, MODULE_WIDTH, PANEL_HEIGHT]);
                
                // centered controls
                translate([-1,MODULE_WIDTH/2,BASE_THICKNESS]){
                    
                    // LED holes (top-down)
                    for(i=[1:LED_COUNT]){
                        translate([0,0,PANEL_HEIGHT-LED_MAX_HEIGHT-(i*LED_SPACING)]){
                            rotate([0,90,0]){
                                cylinder(r=LED_SIZE/2,h=BASE_THICKNESS+2);
                            }
                        }
                    }
                    
                    // Switch holes
                    for(i=[1:SWITCH_COUNT]){
                        translate([0,0,PANEL_HEIGHT-(LED_MAX_HEIGHT + LED_COUNT * LED_SPACING)-(i*SWITCH_SPACING)]){
                            rotate([0,90,0]){
                                cylinder(r=SWITCH_SIZE/2,h=BASE_THICKNESS+2);
                            }
                        }
                    }            
                }
                
                // TODO: USB hole
            }
        }

        // brace
        // TODO: consider cutting some holes in this to reduce
        // material/speed-up print time/make cooler :)
        translate([0,BRACE_THICKNESS,0]){
            rotate([90,0,0]){
                linear_extrude(height=BRACE_THICKNESS){
                    polygon(
                        points=[
                            [BASE_THICKNESS,PANEL_HEIGHT],
                            [PLATE_LENGTH,BASE_THICKNESS],
                            [0,0]
                            //[PLATE_LENGTH,0],
                            //[BASE_THICKNESS,PANEL_HEIGHT-BASE_THICKNESS]
                        ],
                        //paths=[[0,1,2,3]]
                        paths=[[0,1,2]]
                    );
                }
            }
            
            // TODO: LED pin holes
            
            // TODO: switch wiring holes
            
            // TODO: ground bus holes
            
            // TODO: +5vdc power holes
        }

        // bushings 
        // TODO: figure out the right name for these, they're probably not bushings
        // lower
        translate([BASE_THICKNESS+ROD_DIAMETER/2,MODULE_WIDTH,BASE_THICKNESS+ROD_DIAMETER/2]){
            rotate([90,0,0]){
                cylinder(r=(ROD_DIAMETER/2)+BASE_THICKNESS,h=MODULE_WIDTH);
            }    
        }
        // upper
        translate([BASE_THICKNESS+ROD_DIAMETER/2,MODULE_WIDTH,PANEL_HEIGHT-(ROD_DIAMETER/2)-BASE_THICKNESS]){
            rotate([90,0,0]){
                cylinder(r=(ROD_DIAMETER/2)+BASE_THICKNESS,h=MODULE_WIDTH);
            }    
        }
        
        // 40 pin female connector
        translate([BOARD_EDGE_TO_CONNECTOR_DISTANCE,0,BASE_THICKNESS]){
            difference(){
                cube([60,15,7]);
                
                // connector cut-out
                // TODO: there's a lot of unecissary manipulation
                // here to allow the re-use of the cut-out from the
                // old model.  This is lazy, re-work
                translate([0,0,8]){
                    rotate([-90,0,0]){
                        translate([3.5,0,2.5]){
                            cube([56,8,11]);
                        }
                        
                        // header cut-out
                        translate([5.5,0,12]){
                            cube([52,8,5]);
                        }  
                    }
                }
            }
        }
    }
    
    // bushing holes
    // lower
    translate([BASE_THICKNESS+ROD_DIAMETER/2,MODULE_WIDTH+1,BASE_THICKNESS+ROD_DIAMETER/2]){
        rotate([90,0,0]){
            cylinder(r=(ROD_DIAMETER/2)+ROD_CLEARANCE,h=MODULE_WIDTH+2);
            // nut cut-out
            cylinder(r=4.5,h=4);
            translate([-1.5,-5,0]){
                cube([6.5,10,4]);
            }
        }    
    }

    // upper
    translate([BASE_THICKNESS+ROD_DIAMETER/2,MODULE_WIDTH+1,PANEL_HEIGHT-(ROD_DIAMETER/2)-BASE_THICKNESS]){
        rotate([90,0,0]){
            cylinder(r=(ROD_DIAMETER/2)+ROD_CLEARANCE,h=MODULE_WIDTH+2);
            // nut cut-out
            cylinder(r=4.5,h=4);
            translate([-1.5,-5,0]){
                cube([10,10,4]);
            }
        }    
    }
    
    // TODO: cut bushings to accomodate power connectors
}