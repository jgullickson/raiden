// misc notes
// keyboard cad builder: http://builder.swillkb.com/
// keyboard models: https://www.thingiverse.com/thing:1848627/
// display: https://www.pine64.org/?product=7-lcd-touch-screen-panel

// Clusterboard
color("lightgreen"){
    cube([170,170,5]);
}

// keyboard
translate([0,0,10]){
    color("lightblue"){
        translate([0,0,0]){
            import("planck_topplate_part_a.stl", convexity=3);
        }
        translate([79,0,0]){
            import("planck_topplate_part_b.stl", convexity=3);
        }
        translate([156,0,0]){
            import("planck_topplate_part_c.stl", convexity=3);
        }
    }
}

// lcd panel
color("lightsalmon"){
    translate([0,200,0]){
        cube([163.9, 97.1, 2.6]);
    }
}